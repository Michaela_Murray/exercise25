﻿using System;

namespace exercise25
{
    public class Person
    {
        string Name;
        int Age;

        void SayHello()
        {

            Console.WriteLine($"Hello World!");
            Console.WriteLine($"My name is {Name} and I am {Age}!");
        }
        static void Main(string[] args)
        {
          var moo = new Person();

          moo.Name = "Moo";
          moo.Age = 29;
          moo.SayHello(); 

          var a1 = new Checkout(3, 4.50);
          var a2 = new Checkout(2, 6.80);
          var a3 = new Checkout(4, 5.10);

          Console.WriteLine(a1+a2+a3); 
        
        }
        public class Checkout
        {
            int Quantity;
            double Price;

            public Checkout(int _quantity, double _price)
            {
                Quantity = _quantity;
                Price = _quantity;

            }
        }
    }
}

    